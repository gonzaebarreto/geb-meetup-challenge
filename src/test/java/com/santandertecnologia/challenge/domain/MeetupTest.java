package com.santandertecnologia.challenge.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.santandertecnologia.challenge.web.rest.TestUtil;

public class MeetupTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Meetup.class);
        Meetup meetup1 = new Meetup();
        meetup1.setId(1L);
        Meetup meetup2 = new Meetup();
        meetup2.setId(meetup1.getId());
        assertThat(meetup1).isEqualTo(meetup2);
        meetup2.setId(2L);
        assertThat(meetup1).isNotEqualTo(meetup2);
        meetup1.setId(null);
        assertThat(meetup1).isNotEqualTo(meetup2);
    }
}
