package com.santandertecnologia.challenge.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.santandertecnologia.challenge.ChallengebirrasApp;
import com.santandertecnologia.challenge.domain.Ubicacion;
import com.santandertecnologia.challenge.repository.UbicacionRepository;
import com.santandertecnologia.challenge.service.MailService;
import com.santandertecnologia.challenge.service.MeetupService;
import com.santandertecnologia.challenge.service.dto.MeetupDTO;
import com.santandertecnologia.challenge.service.dto.ParticipanteDTO;
import com.santandertecnologia.challenge.service.dto.UbicacionDTO;
import com.santandertecnologia.challenge.service.mapper.UbicacionMapper;
import com.santandertecnologia.challenge.web.rest.errors.FechaNoDisponibleEnPronosticoException;
import com.santandertecnologia.challenge.web.rest.errors.UsuarioNoEstaInscriptoEnMeetupException;
import com.santandertecnologia.challenge.web.rest.vm.LoginVM;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Tests de integración para probar las User Story 6:
 * User Story 6: Como usuario quiero hacer check-in en una meetup para poder avisar que estuve ahí.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChallengebirrasApp.class)
@AutoConfigureMockMvc
public class HacerCheckingEnMeetupIT {

    @Autowired
    private MeetupService meetupService;

    @Autowired
    private MockMvc restMeetupMockMvc;

    @Autowired
    private UbicacionRepository ubicacionRepository;

    @Autowired
    private UbicacionMapper ubicacionMapper;

    @MockBean
    private MailService mailService;

    private final String ENDPOINT_HACER_CHECKIN_EN_MEETUP = "/api/meetups/{meetupId}/participante";
    private final String ENDPOINT_AUTHENTICATE = "/api/authenticate";

    //Usuarios que ya existen en la base de datos en memoria.
    private final String USUARIO_CON_ROL_USER_LOGIN = "user";
    private final String USUARIO_CON_ROL_USER_PASSWORD= "user";

    private final Long ID_MEETUP_EXISTENTE = 1L;

    private ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    /**
     * PUT /api/meetups/{meetupId}/participante Caso 1:
     * Ejecutar servicio con usuario con ROLE_USER con id de participante existente.
     *
     * Resultado esperado: Debe retornar un participante con el checkedIn en TRUE.
     */
    @Test
    @Transactional
    public void hacerCheckinDeParticipanteExistenteEnMeetupExistente() throws Exception {
        ParticipanteDTO participanteDTO = new ParticipanteDTO();
        participanteDTO.setId(5L);

        String responseContent = restMeetupMockMvc
            .perform(put(ENDPOINT_HACER_CHECKIN_EN_MEETUP.replace("{meetupId}",ID_MEETUP_EXISTENTE.toString()))
                .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioUser())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(participanteDTO))
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        ParticipanteDTO responseParticipanteDTO = mapper.readValue(responseContent,ParticipanteDTO.class);

        //Valido que la meetup haya sido creada con id y con el título enviado por parámetro.
        assertNotNull(responseParticipanteDTO);
        assertTrue(responseParticipanteDTO.getCheckedIn());
    }

    /**
     * PUT /api/meetups/{meetupId}/participante Caso 2:
     * Ejecutar servicio con usuario con ROLE_USER con id de participante que no existe en meetup.
     *
     * Resultado esperado: Debe arrojar excepcion UsuarioNoEstaInscriptoEnMeetupException
     */
    @Test
    @Transactional
    public void hacerCheckinDeParticipanteQueNoExisteEnMeetup() throws Exception {
        ParticipanteDTO participanteDTO = new ParticipanteDTO();
        participanteDTO.setId(20L);

        restMeetupMockMvc
            .perform(put(ENDPOINT_HACER_CHECKIN_EN_MEETUP.replace("{meetupId}",ID_MEETUP_EXISTENTE.toString()))
                .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioUser())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(participanteDTO))
            )
            .andExpect(status().isInternalServerError())
            .andExpect(result -> assertTrue(result.getResolvedException() instanceof UsuarioNoEstaInscriptoEnMeetupException));

    }


    private String obtenerTokenUsuarioUser() throws Exception {
        return obtenerTokenUsuario(USUARIO_CON_ROL_USER_LOGIN, USUARIO_CON_ROL_USER_PASSWORD);
    }

    private String obtenerTokenUsuario(String username, String password) throws Exception {
        LoginVM login = new LoginVM();
        login.setUsername(username);
        login.setPassword(password);

        String tokenId = restMeetupMockMvc.perform(post(ENDPOINT_AUTHENTICATE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString()))))
            .andReturn()
            .getResponse()
            .getHeader("Authorization")
            ;

        return tokenId;
    }

}
