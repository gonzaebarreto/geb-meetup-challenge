package com.santandertecnologia.challenge.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.santandertecnologia.challenge.ChallengebirrasApp;
import com.santandertecnologia.challenge.domain.Participante;
import com.santandertecnologia.challenge.domain.Ubicacion;
import com.santandertecnologia.challenge.repository.UbicacionRepository;
import com.santandertecnologia.challenge.service.MailService;
import com.santandertecnologia.challenge.service.MeetupService;
import com.santandertecnologia.challenge.service.dto.MeetupDTO;
import com.santandertecnologia.challenge.service.dto.ParticipanteDTO;
import com.santandertecnologia.challenge.service.dto.UbicacionDTO;
import com.santandertecnologia.challenge.service.mapper.UbicacionMapper;
import com.santandertecnologia.challenge.web.rest.vm.LoginVM;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Tests de integración para probar las siguientes User Stories:
 * User Story 3: Como usuario y como admin quiero poder recibir notificaciones para estar al tanto de las meetups.
 * User Story 4: Como admin quiero armar una meetup para poder invitar otras personas.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChallengebirrasApp.class)
@AutoConfigureMockMvc
public class CrearMeetupIT {

    @Autowired
    private MeetupService meetupService;

    @Autowired
    private MockMvc restMeetupMockMvc;

    @Autowired
    private UbicacionRepository ubicacionRepository;

    @Autowired
    private UbicacionMapper ubicacionMapper;

    @MockBean
    private MailService mailService;

    private final String ENDPOINT_CREAR_MEETUP = "/api/meetups";
    private final String ENDPOINT_AUTHENTICATE = "/api/authenticate";

    //Usuarios que ya existen en la base de datos en memoria.
    private final String USUARIO_CON_ROL_USER_LOGIN = "user";
    private final String USUARIO_CON_ROL_USER_PASSWORD= "user";
    private final String USUARIO_CON_ROL_ADMIN_LOGIN = "admin";
    private final String USUARIO_CON_ROL_ADMIN_PASSWORD = "admin";

    private ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    /**
     * /api/meetups Caso 1:
     * Ejecutar servicio crear-meetup con usuario con ROLE_ADMIN
     *
     * Resultado esperado: Debe registrar una meetup y debe haber notificado a los 19 usuarios existentes de la creacion.
     * de una meetup.
     */
    @Test
    @Transactional
    public void crearMeetupConRoleAdmin() throws Exception {
        MeetupDTO meetupDTO = buildMeetupDTO(getUbicacion1DTO());

        String responseContent = restMeetupMockMvc.perform(post(ENDPOINT_CREAR_MEETUP)
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioAdmin())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(meetupDTO))
        )
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString();

        MeetupDTO responseMeetupDTO = mapper.readValue(responseContent,MeetupDTO.class);

        //Valido que la meetup haya sido creada con id y con el título enviado por parámetro.
        assertNotNull(responseMeetupDTO);
        assertNotNull(responseMeetupDTO.getId());
        assertEquals("Meetup de Test", responseMeetupDTO.getTitulo());
        //Valido que el usuario con role ADMIN que crea la meetup se haya dado de alta como participante.
        assertFalse(responseMeetupDTO.getParticipantes().isEmpty());
        ParticipanteDTO participante = responseMeetupDTO.getParticipantes().get(0);
        assertNotNull(participante);
        assertNotNull(participante.getId());
        assertNotNull(participante.getLogin());
        assertEquals(participante.getLogin(), USUARIO_CON_ROL_ADMIN_LOGIN);

        //Esperamos a que se procesen todos los envios de mail asincronicos.
        verify(mailService, Mockito.timeout(10000).times(19)).sendEmail(anyString(),anyString(),anyString(),anyBoolean(),anyBoolean());
    }

    /**
     * /api/meetups Caso 2:
     * Ejecutar servicio crear-meetup con usuario con ROLE_USER
     *
     * Resultado esperado: Debe dar error 403 Forbidden.
     */
    @Test
    @Transactional
    public void crearMeetupConRoleUserDebeDarForbidden() throws Exception {
        MeetupDTO meetupDTO = buildMeetupDTO(getUbicacion1DTO());

        restMeetupMockMvc.perform(post(ENDPOINT_CREAR_MEETUP)
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioUser())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(meetupDTO))
        )
            .andExpect(status().isForbidden());
    }

    /**
     * /api/meetups Caso 3:
     * Ejecutar servicio crear-meetup con usuario con ROLE_ADMIN y sin ubicacion
     *
     * Resultado esperado: Debe dar error 403 Forbidden.
     */
    @Test
    @Transactional
    public void crearMeetupConRoleAdminSinUbicacionDebeDarError() throws Exception {
        MeetupDTO meetupDTO = buildMeetupDTO(null);

        restMeetupMockMvc.perform(post(ENDPOINT_CREAR_MEETUP)
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioUser())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(meetupDTO))
        )
            .andExpect(status().isForbidden());
    }

    @NotNull
    private MeetupDTO buildMeetupDTO(UbicacionDTO ubicacion) {
        MeetupDTO meetupDTO = new MeetupDTO();
        meetupDTO.setTitulo("Meetup de Test");
        meetupDTO.setDuracionEnMinutos(240);
        meetupDTO.setUbicacion(ubicacion);
        meetupDTO.setFechaHora(ZonedDateTime.of(2020,11,10,10,30,00,00, ZoneId.systemDefault()));
        return meetupDTO;
    }

    private UbicacionDTO getUbicacion1DTO() {
        Optional<Ubicacion> ubicacion = ubicacionRepository.findById(1L);
        assertNotNull(ubicacion.get());
        return ubicacionMapper.toDto(ubicacion.get());
    }


    private String obtenerTokenUsuarioAdmin() throws Exception {
        return obtenerTokenUsuario(USUARIO_CON_ROL_ADMIN_LOGIN, USUARIO_CON_ROL_ADMIN_PASSWORD);
    }

    private String obtenerTokenUsuarioUser() throws Exception {
        return obtenerTokenUsuario(USUARIO_CON_ROL_USER_LOGIN, USUARIO_CON_ROL_USER_PASSWORD);
    }

    private String obtenerTokenUsuario(String username, String password) throws Exception {
        LoginVM login = new LoginVM();
        login.setUsername(username);
        login.setPassword(password);

        String tokenId = restMeetupMockMvc.perform(post(ENDPOINT_AUTHENTICATE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString()))))
            .andReturn()
            .getResponse()
            .getHeader("Authorization")
            ;

        return tokenId;
    }

}
