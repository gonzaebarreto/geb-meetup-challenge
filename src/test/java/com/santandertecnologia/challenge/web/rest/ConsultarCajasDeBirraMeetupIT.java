package com.santandertecnologia.challenge.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.santandertecnologia.challenge.ChallengebirrasApp;
import com.santandertecnologia.challenge.domain.Meetup;
import com.santandertecnologia.challenge.domain.Ubicacion;
import com.santandertecnologia.challenge.service.MeetupService;
import com.santandertecnologia.challenge.service.dto.BirraDTO;
import com.santandertecnologia.challenge.web.rest.errors.FechaNoDisponibleEnPronosticoException;
import com.santandertecnologia.challenge.web.rest.vm.LoginVM;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Tests de integración para probar el funcionamiento de la User Story 1:  Como admin quiero saber cuántas cajas
 * de birras tengo que comprar para poder aprovisionar la meetup.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChallengebirrasApp.class)
@AutoConfigureMockMvc
public class ConsultarCajasDeBirraMeetupIT {

    @Autowired
    private MeetupService meetupService;

    @Autowired
    private MockMvc restMeetupMockMvc;

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    private final String ENDPOINT_CONSULTAR_CAJAS_DE_BIRRA = "/api/meetups/{meetupId}/birra";
    private final String ENDPOINT_AUTHENTICATE = "/api/authenticate";
    private final String URL_API_CON_PRONOSTICO_EXISTENTE = "https://rapidapi.p.rapidapi.com/forecast/daily?lat=%s&lon=%s&lang=ES&metric";

    //Usuarios que ya existen en la base de datos en memoria.
    private final String USUARIO_CON_ROL_USER_LOGIN = "user";
    private final String USUARIO_CON_ROL_USER_PASSWORD= "user";
    private final String USUARIO_CON_ROL_ADMIN_LOGIN = "admin";
    private final String USUARIO_CON_ROL_ADMIN_PASSWORD = "admin";

    private final Long ID_MEETUP_DIA_CON_TT_MENOR_A_20_GRADOS = 1L;
    private final Long ID_MEETUP_DIA_CON_TT_ENTRE_20_Y_24_GRADOS = 2L;
    private final Long ID_MEETUP_DIA_CON_TT_MAYOR_A_20_GRADOS = 3L;
    private final Long ID_MEETUP_INEXISTENTE = 1598L;
    private final Long ID_MEETUP_FECHA_POSTERIOR_AL_PRONOSTICO = 4L;

    private ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    /**
     * /api/meetups/{meetupId}/birra Caso 1:
     * Ejecutar servicio con usuario que tenga rol USER.
     *
     * Resultado esperado: Debe retornar status 403 - Forbidden
     */
    @Test
    @Transactional
    public void ejecutarConsultarCajasDeBirraConUsuarioConRoleUser() throws Exception {
        restMeetupMockMvc.perform(get(ENDPOINT_CONSULTAR_CAJAS_DE_BIRRA.replace("{meetupId}",ID_MEETUP_DIA_CON_TT_MENOR_A_20_GRADOS.toString()))
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioUser())
            .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isForbidden());
    }

    /**
     * /api/meetups/{meetupId}/birra Caso 2:
     * Ejecutar servicio con usuario que tenga rol ADMIN y idMeetup inexistente.
     *
     * Resultado esperado: Debe retornar status 404 - Not Found
     */
    @Test
    @Transactional
    public void ejecutarConsultarCajasDeBirraConIdMeetupInexistente() throws Exception {
        restMeetupMockMvc.perform(get(ENDPOINT_CONSULTAR_CAJAS_DE_BIRRA.replace("{meetupId}",ID_MEETUP_INEXISTENTE.toString()))
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioAdmin())
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNotFound());
    }

    /**
     * /api/meetups/{meetupId}/birra Caso 3:
     * Ejecutar servicio con usuario con rol rol ADMIN y idMeetup con fecha que tenga clima menor a 20 grados.
     *
     * idMeetup 1
     * Cantidad de participantes que tiene: 14
     *
     * Resultado esperado: Debe indicar que deben pedirse 2 cajas de birra ya que se necesitan al menos 11 cervezas (0.75 por participante).
     */
    @Test
    @Transactional
    public void ejecutarConsultarCajasDeBirraConMeetupConTemperaturaMenorA20Grados() throws Exception {
        Optional<Meetup> meetupExistente = meetupService.findOne(ID_MEETUP_DIA_CON_TT_MENOR_A_20_GRADOS);

        String contentResponse = ejecutarConsultarCajasDeBirraConResultadoEsperadoOk(meetupExistente);

        BirraDTO birraDTO = mapper.readValue(contentResponse, BirraDTO.class);

        assertNotNull(birraDTO);
        assertNotNull(birraDTO.getCajasAPedir());
        assertEquals(Integer.valueOf(2),birraDTO.getCajasAPedir());
        assertNotNull(birraDTO.getCervezasNecesarias());
        assertEquals(Integer.valueOf(11),birraDTO.getCervezasNecesarias());
    }

    /**
     * /api/meetups/{meetupId}/birra Caso 4:
     * Ejecutar servicio con usuario con rol rol ADMIN y idMeetup con fecha que tenga clima entre 20 grados y 24 grados.
     *
     * idMeetup 2
     * Cantidad de participantes que tiene: 16
     *
     * Resultado esperado: Debe indicar que deben pedirse 3 cajas de birra ya que se necesitan al menos 16 cervezas (1 por participante).
     */
    @Test
    @Transactional
    public void ejecutarConsultarCajasDeBirraConMeetupConTemperaturEntre20Y24Grados() throws Exception {
        Optional<Meetup> meetupExistente = meetupService.findOne(ID_MEETUP_DIA_CON_TT_ENTRE_20_Y_24_GRADOS);

        String contentResponse = ejecutarConsultarCajasDeBirraConResultadoEsperadoOk(meetupExistente);

        BirraDTO birraDTO = mapper.readValue(contentResponse, BirraDTO.class);

        assertNotNull(birraDTO);
        assertNotNull(birraDTO.getCajasAPedir());
        assertEquals(Integer.valueOf(3),birraDTO.getCajasAPedir());
        assertNotNull(birraDTO.getCervezasNecesarias());
        assertEquals(Integer.valueOf(16),birraDTO.getCervezasNecesarias());
    }

    /**
     * /api/meetups/{meetupId}/birra Caso 5:
     * Ejecutar servicio con usuario con rol rol ADMIN y idMeetup con fecha que tenga clima entre 20 grados y 24 grados.
     *
     * idMeetup 3
     * Cantidad de participantes que tiene: 12
     *
     * Resultado esperado: Debe indicar que deben pedirse 4 cajas de birra ya que se necesitan al menos 24 cervezas (2 por participante).
     */
    @Test
    @Transactional
    public void ejecutarConsultarCajasDeBirraConMeetupConTemperaturMayorA24Grados() throws Exception {
        Optional<Meetup> meetupExistente = meetupService.findOne(ID_MEETUP_DIA_CON_TT_MAYOR_A_20_GRADOS);

        String contentResponse = ejecutarConsultarCajasDeBirraConResultadoEsperadoOk(meetupExistente);

        BirraDTO birraDTO = mapper.readValue(contentResponse, BirraDTO.class);

        assertNotNull(birraDTO);
        assertNotNull(birraDTO.getCajasAPedir());
        assertEquals(Integer.valueOf(4),birraDTO.getCajasAPedir());
        assertNotNull(birraDTO.getCervezasNecesarias());
        assertEquals(Integer.valueOf(24),birraDTO.getCervezasNecesarias());
    }

    /**
     * /api/meetups/{meetupId}/birra Caso 6:
     * Ejecutar servicio con usuario con rol rol ADMIN y idMeetup con fecha que no esté dentro del rango de pronóstico disponible.
     *
     * idMeetup 4
     * Cantidad de participantes que tiene: 1
     *
     * Resultado esperado: Debe arrojar excepcion FechaNoDisponibleEnPronosticoException
     */
    @Test
    @Transactional
    public void ejecutarConsultarCajasDeBirraConMeetupConFechaFueraDelPronostico() throws Exception {
        Optional<Meetup> meetupConFechaPosteriorAlPronosticoDisponible = meetupService.findOne(ID_MEETUP_FECHA_POSTERIOR_AL_PRONOSTICO);
        mockApiExternaDelClimaResponse(meetupConFechaPosteriorAlPronosticoDisponible);
        restMeetupMockMvc.perform(get(ENDPOINT_CONSULTAR_CAJAS_DE_BIRRA.replace("{meetupId}",ID_MEETUP_FECHA_POSTERIOR_AL_PRONOSTICO.toString()))
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioAdmin())
            .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isInternalServerError())
            .andExpect(result -> assertTrue(result.getResolvedException() instanceof FechaNoDisponibleEnPronosticoException));
    }

    private String ejecutarConsultarCajasDeBirraConResultadoEsperadoOk(Optional<Meetup> meetup) throws Exception {
        mockApiExternaDelClimaResponse(meetup);

        String contentResponse = restMeetupMockMvc.perform(get(ENDPOINT_CONSULTAR_CAJAS_DE_BIRRA.replace("{meetupId}",meetup.get().getId().toString()))
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioAdmin())
            .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();
        return contentResponse;
    }

    private void mockApiExternaDelClimaResponse(Optional<Meetup> meetup) throws URISyntaxException, IOException {
        assertTrue(meetup.isPresent());
        Ubicacion ubicacion = meetup.get().getUbicacion();
        assertNotNull(ubicacion);

        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI(String.format(URL_API_CON_PRONOSTICO_EXISTENTE,
                ubicacion.getLatitud(), ubicacion.getLongitud()))))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(respuestaRestClientPronosticoExistente())
            );
    }

    private String obtenerTokenUsuarioAdmin() throws Exception {
        return obtenerTokenUsuario(USUARIO_CON_ROL_ADMIN_LOGIN, USUARIO_CON_ROL_ADMIN_PASSWORD);
    }

    private String obtenerTokenUsuarioUser() throws Exception {
        return obtenerTokenUsuario(USUARIO_CON_ROL_USER_LOGIN, USUARIO_CON_ROL_USER_PASSWORD);
    }

    private String obtenerTokenUsuario(String username, String password) throws Exception {
        LoginVM login = new LoginVM();
        login.setUsername(username);
        login.setPassword(password);

        String tokenId = restMeetupMockMvc.perform(post(ENDPOINT_AUTHENTICATE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString()))))
            .andReturn()
            .getResponse()
            .getHeader("Authorization")
            ;

        return tokenId;
    }

    /**
     * Se lee respuesta dummy desde el archivo respuestaPronosticoClimaApiExterna.json
     * @return
     */
    private String respuestaRestClientPronosticoExistente() throws IOException {
        InputStream inputStream = this.getClass().getResourceAsStream("/respuestaPronosticoClimaApiExterna.json");
        byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
        String respuesta = new String(bdata, StandardCharsets.UTF_8);
        assertNotNull(respuesta);
        assertFalse(respuesta.isEmpty());
        return respuesta;
    }


}
