package com.santandertecnologia.challenge.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.santandertecnologia.challenge.ChallengebirrasApp;
import com.santandertecnologia.challenge.repository.UbicacionRepository;
import com.santandertecnologia.challenge.service.MailService;
import com.santandertecnologia.challenge.service.MeetupService;
import com.santandertecnologia.challenge.service.dto.ParticipanteDTO;
import com.santandertecnologia.challenge.service.dto.UserDTO;
import com.santandertecnologia.challenge.service.mapper.UbicacionMapper;
import com.santandertecnologia.challenge.web.rest.errors.UsuarioYaEstaInscriptoEnMeetupException;
import com.santandertecnologia.challenge.web.rest.vm.LoginVM;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Tests de integración para probar la User Story 5:
 * Como usuario quiero inscribirme en una meetup para poder asistir.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChallengebirrasApp.class)
@AutoConfigureMockMvc
public class InscribirseEnMeetupIT {

    @Autowired
    private MeetupService meetupService;

    @Autowired
    private MockMvc restMeetupMockMvc;

    @Autowired
    private UbicacionRepository ubicacionRepository;

    @Autowired
    private UbicacionMapper ubicacionMapper;

    @MockBean
    private MailService mailService;

    private final String ENDPOINT_INSCRIBIRSE_EN_MEETUP = "/api/meetups/{meetupId}/participante";
    private final String ENDPOINT_AUTHENTICATE = "/api/authenticate";

    //Usuarios que ya existen en la base de datos en memoria.
    private final String USUARIO_CON_ROL_USER_LOGIN = "user";
    private final String USUARIO_CON_ROL_USER_PASSWORD= "user";

    private final Long ID_MEETUP_EXISTENTE = 1L;

    private ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    /**
     * PUT /api/meetups/{meetupId}/participante Caso 1:
     * Ejecutar servicio con usuario con ROLE_USER con id de participante existente en meetup que no tiene a ese participante inscripto.
     *
     * Resultado esperado: Debe retornar un participante.
     */
    @Test
    @Transactional
    public void inscribirseEnMeetupExistente() throws Exception {
        ParticipanteDTO participanteDTO = new ParticipanteDTO();
        participanteDTO.setIdUsuario(20L);

        String responseContent = restMeetupMockMvc
            .perform(post(ENDPOINT_INSCRIBIRSE_EN_MEETUP.replace("{meetupId}",ID_MEETUP_EXISTENTE.toString()))
                .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioUser())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(participanteDTO))
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        ParticipanteDTO responseParticipanteDTO = mapper.readValue(responseContent,ParticipanteDTO.class);

        //Valido que la meetup haya sido creada con id y con el título enviado por parámetro.
        assertNotNull(responseParticipanteDTO);
        assertFalse(responseParticipanteDTO.getCheckedIn());
    }

    /**
     * PUT /api/meetups/{meetupId}/participante Caso 2:
     * Ejecutar servicio con usuario con ROLE_USER con id de participante existente en meetup que no tiene a ese participante inscripto.
     *
     * Resultado esperado: Debe retornar un error que indique que el participante ya estaba inscripto en la meetup.
     */
    @Test
    @Transactional
    public void intentarVolverAInscribirseEnMeetupExistente() throws Exception {
        ParticipanteDTO participanteDTO = new ParticipanteDTO();
        participanteDTO.setIdUsuario(5L);

        restMeetupMockMvc
            .perform(post(ENDPOINT_INSCRIBIRSE_EN_MEETUP.replace("{meetupId}",ID_MEETUP_EXISTENTE.toString()))
                .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioUser())
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(participanteDTO))
            )
            .andExpect(status().isInternalServerError())
            .andExpect(result -> assertTrue(result.getResolvedException() instanceof UsuarioYaEstaInscriptoEnMeetupException));


    }

    private String obtenerTokenUsuarioUser() throws Exception {
        return obtenerTokenUsuario(USUARIO_CON_ROL_USER_LOGIN, USUARIO_CON_ROL_USER_PASSWORD);
    }

    private String obtenerTokenUsuario(String username, String password) throws Exception {
        LoginVM login = new LoginVM();
        login.setUsername(username);
        login.setPassword(password);

        String tokenId = restMeetupMockMvc.perform(post(ENDPOINT_AUTHENTICATE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString()))))
            .andReturn()
            .getResponse()
            .getHeader("Authorization")
            ;

        return tokenId;
    }

}
