package com.santandertecnologia.challenge.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.santandertecnologia.challenge.ChallengebirrasApp;
import com.santandertecnologia.challenge.domain.Meetup;
import com.santandertecnologia.challenge.domain.Ubicacion;
import com.santandertecnologia.challenge.service.MeetupService;
import com.santandertecnologia.challenge.service.dto.TemperaturaDTO;
import com.santandertecnologia.challenge.web.rest.errors.FechaNoDisponibleEnPronosticoException;
import com.santandertecnologia.challenge.web.rest.vm.LoginVM;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Tests de integración para validar el funcionamiento de la User Story 2: Como admin y usuario quiero conocer la
 * temperatura del día de la meetup para saber si va a hacer calor o no.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChallengebirrasApp.class)
@AutoConfigureMockMvc
public class ConsultarTemperaturaMeetupIT {

    @Autowired
    private MeetupService meetupService;

    @Autowired
    private MockMvc restMeetupMockMvc;

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    private final String ENDPOINT_CONSULTAR_TEMPERATURA = "/api/meetups/{meetupId}/temperatura";
    private final String ENDPOINT_AUTHENTICATE = "/api/authenticate";
    private final String URL_API_CON_PRONOSTICO_EXISTENTE = "https://rapidapi.p.rapidapi.com/forecast/daily?lat=%s&lon=%s&lang=ES&metric";

    //Usuarios que ya existen en la base de datos en memoria.
    private final String USUARIO_CON_ROL_USER_LOGIN = "user";
    private final String USUARIO_CON_ROL_USER_PASSWORD= "user";
    private final String USUARIO_CON_ROL_ADMIN_LOGIN = "admin";
    private final String USUARIO_CON_ROL_ADMIN_PASSWORD = "admin";

    private final Long ID_MEETUP_EXISTENTE = 1L;
    private final Long ID_MEETUP_INEXISTENTE = 1598L;
    private final Long ID_MEETUP_FECHA_POSTERIOR_AL_PRONOSTICO = 4L;

    private ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    /**
     * /api/meetups/{meetupId}/temperatura Caso 1:
     * Ejecutar servicio con id de meetup existente y dentro del rango de fechas de pronostico disponibles, como ADMIN.
     * id de Meetup 1 tiene
     *      fecha 27/10/2020 y
     *      ubicacion 1;Edificio Barracas;Av. Paseo Colón y Av. Juan de Garay;-34.6240703;-58.3702155
     *
     * Resultado esperado: Que obtenga el clima de la fecha 27/10/2020
     */
    @Test
    @Transactional
    public void ejecutarConsultarTemperaturaConUsuarioAdmin() throws Exception {
        ejecutarConsultarTemperaturaConFechaMeetupDentroDelPronostico(obtenerTokenUsuarioAdmin());
    }

    /**
     * /api/meetups/{meetupId}/temperatura Caso 2:
     * Ejecutar servicio con id de meetup existente y dentro del rango de fechas de pronostico disponibles, como USUARIO.
     * id de Meetup 1 tiene
     *      fecha 27/10/2020 y
     *      ubicacion 1;Edificio Barracas;Av. Paseo Colón y Av. Juan de Garay;-34.6240703;-58.3702155
     *
     * Resultado esperado: Que obtenga el clima de la fecha 27/10/2020
     */
    @Test
    @Transactional
    public void ejecutarConsultarTemperaturaConUsuarioUser() throws Exception {
        ejecutarConsultarTemperaturaConFechaMeetupDentroDelPronostico(obtenerTokenUsuarioUser());
    }

    private void ejecutarConsultarTemperaturaConFechaMeetupDentroDelPronostico(String tokenUsuario) throws Exception {
        Optional<Meetup> meetupExistente = meetupService.findOne(ID_MEETUP_EXISTENTE);
        assertTrue(meetupExistente.isPresent());
        Ubicacion ubicacion = meetupExistente.get().getUbicacion();
        assertNotNull(ubicacion);

        mockServer.expect(ExpectedCount.once(),
            requestTo(
                new URI(String.format(URL_API_CON_PRONOSTICO_EXISTENTE,ubicacion.getLatitud(),ubicacion.getLongitud()))))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(respuestaRestClientPronosticoExistente())
            );

        String contentResponse = restMeetupMockMvc.perform(get(ENDPOINT_CONSULTAR_TEMPERATURA.replace("{meetupId}",ID_MEETUP_EXISTENTE.toString()))
            .header(HttpHeaders.AUTHORIZATION, tokenUsuario)
            .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        TemperaturaDTO temperaturaDTO = mapper.readValue(contentResponse, TemperaturaDTO.class);

        assertNotNull(temperaturaDTO);
        assertNotNull(temperaturaDTO.getTemperatura());
        assertEquals(Double.valueOf(19.3), temperaturaDTO.getTemperatura());
        assertNotNull(temperaturaDTO.getFecha());
        assertEquals(LocalDate.of(2020,10,27),temperaturaDTO.getFecha());
    }

    /**
     * /api/meetups/{meetupId}/temperatura Caso 3:
     * Ejecutar servicio con id de meetup inexistente.
     * id de Meetup 1598 (no existe)
     *
     * Resultado esperado: Que arroje una excepción elegante
     */
    @Test
    @Transactional
    public void ejecutarConsultarTemperaturaConIdDeMeetupInexistente() throws Exception {
        restMeetupMockMvc.perform(get(ENDPOINT_CONSULTAR_TEMPERATURA.replace("{meetupId}",ID_MEETUP_INEXISTENTE.toString()))
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioAdmin())
            .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNotFound());
    }

    /**
     * /api/meetups/{meetupId}/temperatura Caso 4:
     * Ejecutar servicio sin token de acceso
     *
     * Resultado esperado: Error 403 Forbidden
     */
    @Test
    @Transactional
    public void ejecutarConsultarTemperaturaSinTokenDeAcceso() throws Exception {
        restMeetupMockMvc.perform(get(ENDPOINT_CONSULTAR_TEMPERATURA.replace("{meetupId}",ID_MEETUP_EXISTENTE.toString()))
            .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isUnauthorized());
    }

    /**
     * /api/meetups/{meetupId}/temperatura Caso 5:
     * Ejecutar servicio con id de meetup existente y que su fecha esté fuera del rango de fechas del pronostico disponibles.
     * id de Meetup 10 tiene
     *      fecha 27/11/2020 y
     *      ubicacion 1;Edificio Barracas;Av. Paseo Colón y Av. Juan de Garay;-34.6240703;-58.3702155
     *
     * Resultado esperado: Que arroje error indicando que no aún no hay temperatura disponible para la meetup.
     */
    @Test
    @Transactional
    public void ejecutarConsultarTemperaturaConFechaPosteriorAlaDelPronostico() throws Exception {
        Optional<Meetup> meetupExistente = meetupService.findOne(ID_MEETUP_FECHA_POSTERIOR_AL_PRONOSTICO);
        assertTrue(meetupExistente.isPresent());
        Ubicacion ubicacion = meetupExistente.get().getUbicacion();
        assertNotNull(ubicacion);

        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI(String.format(URL_API_CON_PRONOSTICO_EXISTENTE,ubicacion.getLatitud(),ubicacion.getLongitud()))))
            .andExpect(method(HttpMethod.GET))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(respuestaRestClientPronosticoExistente())
            );

        restMeetupMockMvc.perform(get(ENDPOINT_CONSULTAR_TEMPERATURA.replace("{meetupId}",ID_MEETUP_FECHA_POSTERIOR_AL_PRONOSTICO.toString()))
            .header(HttpHeaders.AUTHORIZATION, obtenerTokenUsuarioAdmin())
            .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isInternalServerError())
            .andExpect(result -> assertTrue(result.getResolvedException() instanceof FechaNoDisponibleEnPronosticoException));
    }


    private String obtenerTokenUsuarioAdmin() throws Exception {
        return obtenerTokenUsuario(USUARIO_CON_ROL_ADMIN_LOGIN, USUARIO_CON_ROL_ADMIN_PASSWORD);
    }

    private String obtenerTokenUsuarioUser() throws Exception {
        return obtenerTokenUsuario(USUARIO_CON_ROL_USER_LOGIN, USUARIO_CON_ROL_USER_PASSWORD);
    }

    private String obtenerTokenUsuario(String username, String password) throws Exception {
        LoginVM login = new LoginVM();
        login.setUsername(username);
        login.setPassword(password);

        String tokenId = restMeetupMockMvc.perform(post(ENDPOINT_AUTHENTICATE)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString()))))
            .andReturn()
            .getResponse()
            .getHeader("Authorization")
            ;

        return tokenId;
    }

    /**
     * Se lee respuesta dummy desde el archivo respuestaPronosticoClimaApiExterna.json
     * @return
     */
    private String respuestaRestClientPronosticoExistente() throws IOException {
        InputStream inputStream = this.getClass().getResourceAsStream("/respuestaPronosticoClimaApiExterna.json");
        byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
        String respuesta = new String(bdata, StandardCharsets.UTF_8);
        assertNotNull(respuesta);
        assertFalse(respuesta.isEmpty());
        return respuesta;
    }


}
