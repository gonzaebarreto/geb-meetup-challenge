package com.santandertecnologia.challenge.domain.listener;

import com.santandertecnologia.challenge.aop.SpringContext;
import com.santandertecnologia.challenge.domain.Meetup;
import com.santandertecnologia.challenge.service.NotificadorEventosMeetupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

/**
 * User Story 3
 * Como usuario y como admin quiero poder recibir notificaciones para estar al tanto de las meetups.
 *
 * Esta clase se encarga de escuchar los eventos de registro y de actualizacion de la entidad Meetup y de notificar a
 * los usuarios con roles ADMIN y USER que estén suscriptos a recibir notificaciones de las meetups.
 */
public class MeetupListener {

    private final Logger log = LoggerFactory.getLogger(MeetupListener.class);

    @PostPersist
    public void onPostPersist(Meetup meetup) {
        log.info("MeetupListener.onPostPersist(): " + meetup);
        SpringContext.getBean(NotificadorEventosMeetupService.class).notificarEventoMeetupNueva(meetup);
    }

    @PostUpdate
    public void onPostUpdate(Meetup meetup) {
        log.info("MeetupListener.onPostUpdate(): " + meetup);
        SpringContext.getBean(NotificadorEventosMeetupService.class).notificarEventoMeetupActualizada(meetup);
    }

}
