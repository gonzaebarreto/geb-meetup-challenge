package com.santandertecnologia.challenge.domain;

import com.santandertecnologia.challenge.aop.SpringContext;
import com.santandertecnologia.challenge.service.dto.UbicacionDTO;
import com.santandertecnologia.challenge.service.mapper.UbicacionMapper;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Ubicacion.
 */
@Entity
@Table(name = "ubicacion")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Ubicacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "direccion")
    private String direccion;

    @NotNull
    @Column(name = "latitud", nullable = false)
    private Float latitud;

    @NotNull
    @Column(name = "longitud", nullable = false)
    private Float longitud;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Ubicacion nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public Ubicacion direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Float getLatitud() {
        return latitud;
    }

    public Ubicacion latitud(Float latitud) {
        this.latitud = latitud;
        return this;
    }

    public void setLatitud(Float latitud) {
        this.latitud = latitud;
    }

    public Float getLongitud() {
        return longitud;
    }

    public Ubicacion longitud(Float longitud) {
        this.longitud = longitud;
        return this;
    }

    public void setLongitud(Float longitud) {
        this.longitud = longitud;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ubicacion)) {
            return false;
        }
        return id != null && id.equals(((Ubicacion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ubicacion{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", direccion='" + getDireccion() + "'" +
            ", latitud=" + getLatitud() +
            ", longitud=" + getLongitud() +
            "}";
    }

    public UbicacionDTO toDTO() {
        return SpringContext.getBean(UbicacionMapper.class).toDto(this);
    }
}
