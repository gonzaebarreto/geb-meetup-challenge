package com.santandertecnologia.challenge.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.santandertecnologia.challenge.aop.SpringContext;
import com.santandertecnologia.challenge.repository.ParticipanteRepository;
import com.santandertecnologia.challenge.service.dto.ParticipanteDTO;
import com.santandertecnologia.challenge.service.mapper.ParticipanteMapper;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Participante.
 */
@Entity
@Table(name = "participante")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Participante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "checked_in", nullable = false)
    private Boolean checkedIn = Boolean.FALSE;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "participantes", allowSetters = true)
    private User user;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "participantes", allowSetters = true)
    private Meetup meetup;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isCheckedIn() {
        return checkedIn;
    }

    public Participante checkedIn(Boolean checkedIn) {
        this.checkedIn = checkedIn;
        return this;
    }

    public void setCheckedIn(Boolean checkedIn) {
        this.checkedIn = checkedIn;
    }

    public User getUser() {
        return user;
    }

    public Participante user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Meetup getMeetup() {
        return meetup;
    }

    public Participante meetup(Meetup meetup) {
        this.meetup = meetup;
        return this;
    }

    public void setMeetup(Meetup meetup) {
        this.meetup = meetup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Participante)) {
            return false;
        }
        return id != null && id.equals(((Participante) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Participante{" +
            "id=" + getId() +
            ", checkedIn='" + isCheckedIn() + "'" +
            "}";
    }

    public ParticipanteDTO toDTO() {
        return SpringContext.getBean(ParticipanteMapper.class).toDto(this);
    }

    public Participante save() {
        return SpringContext.getBean(ParticipanteRepository.class).save(this);
    }
}
