package com.santandertecnologia.challenge.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.santandertecnologia.challenge.aop.SpringContext;
import com.santandertecnologia.challenge.domain.listener.MeetupListener;
import com.santandertecnologia.challenge.service.ClimaService;
import com.santandertecnologia.challenge.service.dto.BirraDTO;
import com.santandertecnologia.challenge.service.dto.MeetupDTO;
import com.santandertecnologia.challenge.service.dto.TemperaturaDTO;
import com.santandertecnologia.challenge.service.mapper.MeetupMapper;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * A Meetup.
 */
@Entity
@Table(name = "meetup")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EntityListeners(MeetupListener.class)
public class Meetup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "fecha_hora", nullable = false)
    private ZonedDateTime fechaHora;

    @NotNull
    @Column(name = "titulo", nullable = false)
    private String titulo;

    @Column(name = "duracion_en_minutos")
    private Integer duracionEnMinutos;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "meetups", allowSetters = true)
    private Ubicacion ubicacion;

    @OneToMany(mappedBy = "meetup", cascade = CascadeType.PERSIST)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Participante> participantes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFechaHora() {
        return fechaHora;
    }

    public Meetup fechaHora(ZonedDateTime fechaHora) {
        this.fechaHora = fechaHora;
        return this;
    }

    public void setFechaHora(ZonedDateTime fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getTitulo() {
        return titulo;
    }

    public Meetup titulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getDuracionEnMinutos() {
        return duracionEnMinutos;
    }

    public Meetup duracionEnMinutos(Integer duracionEnMinutos) {
        this.duracionEnMinutos = duracionEnMinutos;
        return this;
    }

    public void setDuracionEnMinutos(Integer duracionEnMinutos) {
        this.duracionEnMinutos = duracionEnMinutos;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public Meetup ubicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
        return this;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Set<Participante> getParticipantes() {
        return participantes;
    }

    public Meetup participantes(Set<Participante> participantes) {
        this.participantes = participantes;
        return this;
    }

    public Meetup addParticipantes(Participante participante) {
        this.participantes.add(participante);
        participante.setMeetup(this);
        return this;
    }

    public Meetup removeParticipantes(Participante participante) {
        this.participantes.remove(participante);
        participante.setMeetup(null);
        return this;
    }

    public void setParticipantes(Set<Participante> participantes) {
        this.participantes = participantes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Meetup)) {
            return false;
        }
        return id != null && id.equals(((Meetup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Meetup{" +
            "id=" + getId() +
            ", fechaHora='" + getFechaHora() + "'" +
            ", titulo='" + getTitulo() + "'" +
            ", duracionEnMinutos=" + getDuracionEnMinutos() +
            "}";
    }

    public TemperaturaDTO consultarTemperatura() {
        return SpringContext.getBean(ClimaService.class)
            .consultarTemperaturaMaximaDelDia(this.getFechaHora().toLocalDate(),this.getUbicacion());
    }

    /**
     * Este método determina la cantidad de cajas de birra a pedir para el meetup de acuerdo a la cantidad de
     * participantes y a la temperatura del día de la meetup.
     * 1 caja de cerveza trae 6 unidades. Con lo cual primero se cuenta la cantidad de birra necesaria y luego se divide
     * por 6 para calcular la cantidad de cajas. Siempre se redondea para arriba (preferimos que sobre y no que falte)
     * @return
     */
    public BirraDTO calcularCantidadDeBirras() {
        Integer cantidadParticipantes = this.getParticipantes().size();
        Double temperaturaMeetup = this.consultarTemperatura().getTemperatura();

        Integer cantidadDeBirrasNecesarias = determinarCantidadDeBirrasNecesarias(cantidadParticipantes, temperaturaMeetup);
        Integer cantidadDeCajasDeBirraAPedir  = determinarCantidadDeCajasDeBirraAPedir(cantidadDeBirrasNecesarias);

        return new BirraDTO()
                    .cajasAPedir(cantidadDeCajasDeBirraAPedir)
                    .cervezasNecesarias(cantidadDeBirrasNecesarias)
                    .meetupId(this.getId());
    }

    private MeetupDTO toDTO() {
        return SpringContext.getBean(MeetupMapper.class).toDto(this);
    }

    /**
     * Se agrupa la cantidad de birra necesaria en cajas de 6 unidades.
     * @param cantidadDeBirrasNecesarias
     * @return
     */
    private int determinarCantidadDeCajasDeBirraAPedir(Integer cantidadDeBirrasNecesarias) {
        return Double.valueOf(Math.ceil((double)cantidadDeBirrasNecesarias / 6)).intValue();
    }

    /**
     * Determinar la cantidad de birras necesarias de acuerdo a la cantidad de participantes y la temperatura teniendo en cuenta:
     * Si hace entre 20 y 24 grados, se toma una birra por persona;
     * si hace menos de 20 grados, se toma 0.75;
     * y si hace mucho calor (más de 24 grados), se toman 2 birras más por persona.
     * Y siempre preferimos que sobre y no que falte.
     * @param cantidadParticipantes
     * @param temperaturaMeetup
     * @return
     */
    private Integer determinarCantidadDeBirrasNecesarias(Integer cantidadParticipantes, Double temperaturaMeetup) {
        Integer cantidadDeBirrasTotal;
        if (temperaturaMeetup < 20) {
            cantidadDeBirrasTotal = Double.valueOf(Math.ceil(cantidadParticipantes * 0.75)).intValue();
        } else if(temperaturaMeetup >=20 && temperaturaMeetup <24) {
            cantidadDeBirrasTotal = cantidadParticipantes;
        } else {
            cantidadDeBirrasTotal = cantidadParticipantes * 2;
        }
        return cantidadDeBirrasTotal;
    }

    public boolean usuarioEstaInscriptoComoParticipante(Long idUser) {
        Optional<Participante> optionalParticipante = this.getParticipantes()
                                                            .stream()
                                                            .filter(participante -> participante.getUser().getId().equals(idUser))
                                                            .findFirst();
        return optionalParticipante.isPresent();
    }
}
