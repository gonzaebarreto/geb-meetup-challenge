package com.santandertecnologia.challenge.repository;

import com.santandertecnologia.challenge.domain.Participante;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Participante entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParticipanteRepository extends JpaRepository<Participante, Long> {

    @Query("select participante from Participante participante where participante.user.login = ?#{principal.username}")
    List<Participante> findByUserIsCurrentUser();
}
