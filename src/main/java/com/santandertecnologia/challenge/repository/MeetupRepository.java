package com.santandertecnologia.challenge.repository;

import com.santandertecnologia.challenge.domain.Meetup;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Meetup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeetupRepository extends JpaRepository<Meetup, Long> {
}
