package com.santandertecnologia.challenge.service;

import com.santandertecnologia.challenge.domain.Meetup;
import com.santandertecnologia.challenge.domain.Participante;
import com.santandertecnologia.challenge.domain.Ubicacion;
import com.santandertecnologia.challenge.domain.User;
import com.santandertecnologia.challenge.repository.MeetupRepository;
import com.santandertecnologia.challenge.repository.ParticipanteRepository;
import com.santandertecnologia.challenge.service.dto.BirraDTO;
import com.santandertecnologia.challenge.service.dto.MeetupDTO;
import com.santandertecnologia.challenge.service.dto.ParticipanteDTO;
import com.santandertecnologia.challenge.service.dto.UserDTO;
import com.santandertecnologia.challenge.service.mapper.MeetupMapper;
import com.santandertecnologia.challenge.web.rest.errors.NoExisteUbicacionException;
import com.santandertecnologia.challenge.web.rest.errors.UsuarioNoEstaInscriptoEnMeetupException;
import com.santandertecnologia.challenge.web.rest.errors.UsuarioYaEstaInscriptoEnMeetupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Meetup}.
 */
@Service
@Transactional
public class MeetupService {

    private final Logger log = LoggerFactory.getLogger(MeetupService.class);

    private final MeetupRepository meetupRepository;

    private final MeetupMapper meetupMapper;

    private final UbicacionService ubicacionService;

    private final UserService userService;

    public MeetupService(MeetupRepository meetupRepository,
                         MeetupMapper meetupMapper,
                         UbicacionService ubicacionService,
                         UserService userService) {
        this.meetupRepository = meetupRepository;
        this.meetupMapper = meetupMapper;
        this.ubicacionService = ubicacionService;
        this.userService = userService;
    }

    /**
     * Save a meetup.
     *
     * @param meetup the entity to save.
     * @return the persisted entity.
     */
    public Meetup save(Meetup meetup) {
        log.debug("Request to save Meetup : {}", meetup);
        return meetupRepository.save(meetup);
    }

    /**
     * Get one meetup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Meetup> findOne(Long id) {
        log.debug("Request to get Meetup : {}", id);
        return meetupRepository.findById(id);
    }

    /**
     * Crea la meetup y envia las invitaciones a usuarios
     * @param meetupDTO
     * @return
     */
    public MeetupDTO crearMeetup(MeetupDTO meetupDTO) {
        Optional<Ubicacion> ubicacionOptional = ubicacionService.findOne(meetupDTO.getUbicacion().getId());
        //Validar existencia
        if(!ubicacionOptional.isPresent()) {
            throw new NoExisteUbicacionException(meetupDTO.getUbicacion().getId());
        }
        Optional<User> userOptional = userService.getUserWithAuthoritiesByLogin(SecurityContextHolder.getContext()
            .getAuthentication()
            .getName());
        Meetup meetup = new Meetup()
            .fechaHora(meetupDTO.getFechaHora())
            .titulo(meetupDTO.getTitulo())
            .duracionEnMinutos(meetupDTO.getDuracionEnMinutos())
            .ubicacion(ubicacionOptional.get())
            .addParticipantes(new Participante().user(userOptional.get()));

        this.save(meetup);

        return meetupMapper.toDto(meetup);
    }

    public ParticipanteDTO hacerCheckingDeParticipante(Meetup meetup, Long idParticipante) {
        Optional<Participante> optionalParticipante = meetup.getParticipantes()
                                                            .stream()
                                                            .filter(participante -> participante.getId().equals(idParticipante))
                                                            .findFirst();
        if (optionalParticipante.isPresent()) {
            optionalParticipante.get().setCheckedIn(Boolean.TRUE);
            return optionalParticipante.get().toDTO();
        } else {
            throw new UsuarioNoEstaInscriptoEnMeetupException(meetup.getId());
        }
    }

    public ParticipanteDTO inscribirParticipanteAMeetup(Meetup meetup, Long idUser, String login) {
        if(meetup.usuarioEstaInscriptoComoParticipante(idUser)) {
            throw new UsuarioYaEstaInscriptoEnMeetupException();
        }

        Optional<User> user = userService.obtenerUserPorIdOLogin(idUser,login);
        Participante participante = new Participante();
        if(user.isPresent()) {
            meetup.addParticipantes(participante.user(user.get()));
            participante.save();
            this.save(meetup);
        }

        return participante.toDTO();
    }

    @Transactional
    public BirraDTO consultarCantidadDeBirrasAComprar(Long meetupId) {
        Optional<Meetup> optionalMeetup = this.findOne(meetupId);
        if(optionalMeetup.isPresent()) {
            return optionalMeetup.get().calcularCantidadDeBirras();
        }
        return null;
    }

}
