package com.santandertecnologia.challenge.service;

import com.santandertecnologia.challenge.config.Constants;
import com.santandertecnologia.challenge.domain.User;
import com.santandertecnologia.challenge.repository.UserRepository;
import com.santandertecnologia.challenge.service.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesById(Long idUser) {
        return userRepository.findOneWithAuthoritiesById(idUser);
    }

    @Transactional(readOnly = true)
    public Optional<User> obtenerUserPorIdOLogin(Long idUser, String login) {
        if(idUser!=null) {
            return getUserWithAuthoritiesById(idUser);
        } else {
            return getUserWithAuthoritiesByLogin(login);
        }
    }
}
