package com.santandertecnologia.challenge.service.mapper;

import com.santandertecnologia.challenge.domain.Meetup;
import com.santandertecnologia.challenge.service.dto.MeetupCreadaDTO;
import com.santandertecnologia.challenge.service.dto.MeetupDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",uses = {UbicacionMapper.class, ParticipanteMapper.class})
public interface MeetupMapper extends EntityMapper<MeetupDTO,Meetup>{


}
