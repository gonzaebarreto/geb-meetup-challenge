package com.santandertecnologia.challenge.service.mapper;

import com.santandertecnologia.challenge.domain.Ubicacion;
import com.santandertecnologia.challenge.service.dto.UbicacionDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface UbicacionMapper extends EntityMapper<UbicacionDTO, Ubicacion> {

}
