package com.santandertecnologia.challenge.service.mapper;

import com.santandertecnologia.challenge.service.dto.ClimaPorDiaDTO;
import com.santandertecnologia.challenge.service.dto.TemperaturaDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",uses = {})
public interface TemperaturaMapper {

    @Mapping(target = "temperatura", source = "temperaturaMaxima")
    @Mapping(target = "fecha", source = "fecha")
    @Mapping(target = "descripcionClima", source = "descripcionClima.descripcion")
    TemperaturaDTO toTemperaturaDTO(ClimaPorDiaDTO climaPorDiaDTO);

}
