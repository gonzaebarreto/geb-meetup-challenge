package com.santandertecnologia.challenge.service.mapper;

import com.santandertecnologia.challenge.domain.Participante;
import com.santandertecnologia.challenge.service.dto.ParticipanteDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface ParticipanteMapper extends EntityMapper<ParticipanteDTO, Participante> {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "checkedIn", source = "checkedIn")
    @Mapping(target = "idUsuario", source = "user.id")
    @Mapping(target = "login", source = "user.login")
    @Mapping(target = "nombre", source = "user.firstName")
    @Mapping(target = "apellido", source = "user.lastName")
    @Mapping(target = "email", source = "user.email")
    ParticipanteDTO toDto(Participante entity);

}
