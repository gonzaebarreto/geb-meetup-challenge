package com.santandertecnologia.challenge.service.mapper;

import java.util.List;

/**
 * Contrato genérico para pasar de una entidad de domino a un DTO y viceversa.
 *
 * @param <D> - parámetro con el tipo de DTO.
 * @param <E> - paráemtro con el tipo de la Entidad.
 */

public interface EntityMapper<D, E> {

    E toEntity(D dto);

    D toDto(E entity);

    List <E> toEntity(List<D> dtoList);

    List <D> toDto(List<E> entityList);
}
