package com.santandertecnologia.challenge.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PronosticoClimaDTO {
    @JsonProperty(value = "city_name")
    private String ciudad;
    @JsonProperty(value = "country_code")
    private String codigoPais;
    @JsonProperty(value = "lon")
    private Double longitud;
    @JsonProperty(value = "lat")
    private Double latitud;
    @JsonProperty(value = "data")
    private List<ClimaPorDiaDTO> climaPorDias;

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public List<ClimaPorDiaDTO> getClimaPorDias() {
        return climaPorDias;
    }

    public void setClimaPorDias(List<ClimaPorDiaDTO> climaPorDias) {
        this.climaPorDias = climaPorDias;
    }

    @Override
    public String toString() {
        return "PronosticoClimaDTO{" +
            "ciudad='" + ciudad + '\'' +
            ", codigoPais='" + codigoPais + '\'' +
            ", longitud=" + longitud +
            ", latitud=" + latitud +
            ", climaPorDias=" + climaPorDias +
            '}';
    }
}
