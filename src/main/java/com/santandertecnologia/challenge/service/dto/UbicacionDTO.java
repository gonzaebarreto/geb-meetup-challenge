package com.santandertecnologia.challenge.service.dto;

public class UbicacionDTO {

    private Long id;
    private String nombre;
    private String direccion;
    private Double latitud;
    private Double longitud;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "UbicacionDTO{" +
            "id=" + id +
            ", nombre='" + nombre + '\'' +
            ", direccion='" + direccion + '\'' +
            ", latitud=" + latitud +
            ", longitud=" + longitud +
            '}';
    }
}
