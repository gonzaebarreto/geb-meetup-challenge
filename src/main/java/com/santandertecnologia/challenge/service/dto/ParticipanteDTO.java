package com.santandertecnologia.challenge.service.dto;

import org.apache.commons.lang3.StringUtils;

public class ParticipanteDTO {

    private Long id;
    private Boolean checkedIn;
    private Long idUsuario;
    private String login;
    private String nombre;
    private String apellido;
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getCheckedIn() {
        return checkedIn;
    }

    public void setCheckedIn(Boolean checkedIn) {
        this.checkedIn = checkedIn;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "ParticipanteDTO{" +
            "id=" + id +
            ", checkedIn=" + checkedIn +
            ", nombre='" + nombre + '\'' +
            ", apellido='" + apellido + '\'' +
            ", email='" + email + '\'' +
            '}';
    }

    public boolean noTieneValorEnIdNiEnLogin() {
        return this.getIdUsuario() == null && StringUtils.isBlank(this.getLogin());
    }
}
