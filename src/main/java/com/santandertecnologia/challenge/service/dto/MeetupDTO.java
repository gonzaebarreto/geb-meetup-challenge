package com.santandertecnologia.challenge.service.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class MeetupDTO {

    private Long id;

    private ZonedDateTime fechaHora;

    private String titulo;

    private Integer duracionEnMinutos;

    private UbicacionDTO ubicacion;

    private List<ParticipanteDTO> participantes = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(ZonedDateTime fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getDuracionEnMinutos() {
        return duracionEnMinutos;
    }

    public void setDuracionEnMinutos(Integer duracionEnMinutos) {
        this.duracionEnMinutos = duracionEnMinutos;
    }

    public UbicacionDTO getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(UbicacionDTO ubicacion) {
        this.ubicacion = ubicacion;
    }

    public List<ParticipanteDTO> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(List<ParticipanteDTO> participantes) {
        this.participantes = participantes;
    }

    @Override
    public String toString() {
        return "MeetupDTO{" +
            "id=" + id +
            ", fechaHora=" + fechaHora +
            ", titulo='" + titulo + '\'' +
            ", duracionEnMinutos=" + duracionEnMinutos +
            ", ubicacion=" + ubicacion +
            ", participantes=" + participantes +
            '}';
    }
}
