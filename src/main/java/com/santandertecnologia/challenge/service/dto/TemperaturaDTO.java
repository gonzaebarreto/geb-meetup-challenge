package com.santandertecnologia.challenge.service.dto;

import java.time.LocalDate;

public class TemperaturaDTO {

    private LocalDate fecha;
    private Double temperatura;
    private String descripcionClima;

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }

    public String getDescripcionClima() {
        return descripcionClima;
    }

    public void setDescripcionClima(String descripcionClima) {
        this.descripcionClima = descripcionClima;
    }

    @Override
    public String toString() {
        return "TemperaturaDTO{" +
            "fecha=" + fecha +
            ", temperatura=" + temperatura +
            ", descripcionClima='" + descripcionClima + '\'' +
            '}';
    }


}
