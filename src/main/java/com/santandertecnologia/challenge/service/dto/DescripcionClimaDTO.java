package com.santandertecnologia.challenge.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DescripcionClimaDTO {
    @JsonProperty(value = "description")
    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public DescripcionClimaDTO descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    @Override
    public String toString() {
        return "DescripcionClimaDTO{" +
            "descripcion='" + descripcion + '\'' +
            '}';
    }
}
