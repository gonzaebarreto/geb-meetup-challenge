package com.santandertecnologia.challenge.service.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class ClimaPorDiaDTO {
    @JsonProperty(value = "max_temp")
    private Double temperaturaMaxima;
    @JsonProperty(value = "min_temp")
    private Double temperaturaMinima;
    @JsonProperty(value = "temp")
    private Double temperatura;
    @JsonProperty(value = "datetime")
    private LocalDate fecha;
    @JsonProperty(value = "weather")
    private DescripcionClimaDTO descripcionClima;

    public Double getTemperaturaMaxima() {
        return temperaturaMaxima;
    }

    public void setTemperaturaMaxima(Double temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }

    public Double getTemperaturaMinima() {
        return temperaturaMinima;
    }

    public void setTemperaturaMinima(Double temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    public Double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public DescripcionClimaDTO getDescripcionClima() {
        return descripcionClima;
    }

    public void setDescripcionClima(DescripcionClimaDTO descripcionClima) {
        this.descripcionClima = descripcionClima;
    }

    public ClimaPorDiaDTO temperaturaMaxima(Double temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
        return this;
    }

    public ClimaPorDiaDTO temperaturaMinima(Double temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
        return this;
    }

    public ClimaPorDiaDTO temperatura(Double temperatura) {
        this.temperatura = temperatura;
        return this;
    }

    public ClimaPorDiaDTO fecha(LocalDate fecha) {
        this.fecha = fecha;
        return this;
    }

    public ClimaPorDiaDTO descripcionClima(DescripcionClimaDTO descripcionClima) {
        this.descripcionClima = descripcionClima;
        return this;
    }

    @Override
    public String toString() {
        return "ClimaPorDiaDTO{" +
            "temperaturaMaxima=" + temperaturaMaxima +
            ", temperaturaMinima=" + temperaturaMinima +
            ", temperatura=" + temperatura +
            ", fecha=" + fecha +
            ", descripcionClima=" + descripcionClima +
            '}';
    }
}
