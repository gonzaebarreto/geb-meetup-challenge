package com.santandertecnologia.challenge.service.dto;

import java.time.ZonedDateTime;

public class MeetupCreadaDTO {

    private Long idMeetup;
    private ZonedDateTime fechaHora;

    public Long getIdMeetup() {
        return idMeetup;
    }

    public void setIdMeetup(Long idMeetup) {
        this.idMeetup = idMeetup;
    }

    public ZonedDateTime getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(ZonedDateTime fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public String toString() {
        return "MeetupCreadaDTO{" +
            "idMeetup=" + idMeetup +
            ", fechaHora=" + fechaHora +
            '}';
    }
}
