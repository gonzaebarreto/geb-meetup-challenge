package com.santandertecnologia.challenge.service.dto;

public class BirraDTO {

    private Integer cajasAPedir;
    private Integer cervezasNecesarias;
    private Long meetupId;

    public Integer getCajasAPedir() {
        return cajasAPedir;
    }

    public void setCajasAPedir(Integer cajasAPedir) {
        this.cajasAPedir = cajasAPedir;
    }

    public Integer getCervezasNecesarias() {
        return cervezasNecesarias;
    }

    public void setCervezasNecesarias(Integer cervezasNecesarias) {
        this.cervezasNecesarias = cervezasNecesarias;
    }


    public BirraDTO cajasAPedir(Integer cajasAPedir) {
        this.cajasAPedir = cajasAPedir;
        return this;
    }

    public BirraDTO cervezasNecesarias(Integer cervezasNecesarias) {
        this.cervezasNecesarias = cervezasNecesarias;
        return this;
    }

    public BirraDTO meetupId(Long meetupId) {
        this.meetupId = meetupId;
        return this;
    }

    @Override
    public String toString() {
        return "BirraDTO{" +
            ", cajasAPedir=" + cajasAPedir +
            ", cervezasNecesarias=" + cervezasNecesarias +
            ", meetupId=" + meetupId +
            '}';
    }
}
