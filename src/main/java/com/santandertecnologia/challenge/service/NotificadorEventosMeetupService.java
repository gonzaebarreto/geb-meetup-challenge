package com.santandertecnologia.challenge.service;

import com.santandertecnologia.challenge.domain.Meetup;
import com.santandertecnologia.challenge.security.AuthoritiesConstants;
import com.santandertecnologia.challenge.service.dto.UserDTO;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Esta clase se encarga de levantar aquellos usuarios que tengan ROL_USER y ROL_ADMIN y les envia un mail con
 * un mensaje que indicando que se creó o se actualizó una meetup.
 */
@Async
@Service
public class NotificadorEventosMeetupService {

    private final UserService userService;
    private final MailService mailService;

    private final String ASUNTO_MEETUP_CREADA = "Meetup Creada! - %s";
    private final String CUERPO_MEETUP_CREADA = "El día %s se realizará la meetup %s, te esperamos!\n Te dejemos el codigo de meetup %s para que puedas inscribirte!";
    private final String ASUNTO_MEETUP_ACTUALIZADA = "Meetup %s Actualizada!";
    private final String CUERPO_MEETUP_ACTUALIZADA = "Actualizaciones a la meetup %s:\nFecha y hora:%s\nId Meetup: %s";


    public NotificadorEventosMeetupService(UserService userService, MailService mailService) {
        this.userService = userService;
        this.mailService = mailService;
    }

    /**
     * Notifica a los usuarios que corresponda la creacion de una meetup.
     * @param meetup
     */
    public void notificarEventoMeetupNueva(Meetup meetup) {
        notificarAUsuarios(String.format(ASUNTO_MEETUP_CREADA,meetup.getTitulo()),
                           String.format(CUERPO_MEETUP_CREADA,meetup.getFechaHora(), meetup.getTitulo(), meetup.getId()));
    }

    /**
     * Notifica a los usuarios que corresponda la actualizacion de una meetup.
     * @param meetup
     */
    public void notificarEventoMeetupActualizada(Meetup meetup) {
        notificarAUsuarios(String.format(ASUNTO_MEETUP_ACTUALIZADA,meetup.getTitulo()),
                           String.format(CUERPO_MEETUP_ACTUALIZADA,meetup.getFechaHora(), meetup.getTitulo(), meetup.getId()));
    }

    /**
     * Envia un correo a los usuarios notificables.
     * @param asunto
     * @param cuerpo
     */
    private void notificarAUsuarios(String asunto, String cuerpo) {
        Set<UserDTO> usuariosNotificablesDTO = obtenerUsuariosNotificables();
        for(UserDTO usuarioNotificable:usuariosNotificablesDTO) {
            mailService.sendEmail(usuarioNotificable.getEmail(),asunto,cuerpo,false, false);
        }
    }

    /**
     * Un usuario es notificable si tiene mail, si tiene ROL_USER, ROL_ADMIN, y no se trata del anonymous user.
     * @return
     */
    private Set<UserDTO> obtenerUsuariosNotificables() {
        return userService.getAllManagedUsers(Pageable.unpaged())
                    .stream()
                    .filter(userDTO -> StringUtils.isNotBlank(userDTO.getEmail()))
                    .filter(userDTO -> userDTO.getAuthorities().contains(AuthoritiesConstants.ADMIN) ||
                                       userDTO.getAuthorities().contains(AuthoritiesConstants.USER))
                    .collect(Collectors.toSet());
    }
}
