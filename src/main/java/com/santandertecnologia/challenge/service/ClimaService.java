package com.santandertecnologia.challenge.service;

import com.santandertecnologia.challenge.domain.Ubicacion;
import com.santandertecnologia.challenge.service.dto.PronosticoClimaDTO;
import com.santandertecnologia.challenge.service.dto.TemperaturaDTO;
import com.santandertecnologia.challenge.service.mapper.TemperaturaMapper;
import com.santandertecnologia.challenge.web.rest.errors.FechaNoDisponibleEnPronosticoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.*;
import org.springframework.retry.annotation.CircuitBreaker;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

@Service
@Configuration
public class ClimaService {

    @Value("${clima.api.key-header}")
    private String API_KEY_HEADER;
    @Value("${clima.api.key-value}")
    private String API_KEY_VALUE;
    @Value("${clima.api.host-header}")
    private String API_HOST_HEADER;
    @Value("${clima.api.host-value}")
    private String API_HOST_VALUE;
    @Value("${clima.api.forecast-service.url}")
    private String API_WEATHER_SERVICE_URL;
    @Value("${clima.api.forecast-service.language}")
    private String API_LANGUAGE;
    @Value("${clima.api.forecast-service.units}")
    private String API_METRIC_UNITS;

    private final RetryTemplate retryTemplate;

    private final TemperaturaMapper temperaturaMapper;

    private final RestTemplate restTemplate;

    private final Logger log = LoggerFactory.getLogger(ClimaService.class);

    public ClimaService(RetryTemplate retryTemplate,
                        TemperaturaMapper temperaturaMapper,
                        RestTemplate restTemplate) {
        this.retryTemplate = retryTemplate;
        this.temperaturaMapper = temperaturaMapper;
        this.restTemplate = restTemplate;
    }

    /**
     * Dada una fecha y una ubicación consulta la temperatura máxima del día consumiendo una API de Clima externa
     * @param fecha
     * @param ubicacion
     * @return Un Optional que puede contener un objeto TemperaturaDTO con el valor de temperatura máxima del día.
     */
    public TemperaturaDTO consultarTemperaturaMaximaDelDia(LocalDate fecha, Ubicacion ubicacion) {
        ResponseEntity<PronosticoClimaDTO> response = consultarPronosticoDelClima(ubicacion);

        Optional<TemperaturaDTO> optionalTemperaturaDTO =  response.getBody()
                                                                .getClimaPorDias()
                                                                .stream()
                                                                .filter(climaPorDiaDTO -> climaPorDiaDTO.getFecha().equals(fecha))
                                                                .map(climaPorDiaDTO -> temperaturaMapper.toTemperaturaDTO(climaPorDiaDTO))
                                                                .findAny();
        if(!optionalTemperaturaDTO.isPresent()) {
            throw new FechaNoDisponibleEnPronosticoException(fecha);
        } else {
            return optionalTemperaturaDTO.get();
        }

    }

    /**
     * Consulta el pronóstico del clima usando el pattern circuit breaker y reintentos.
     * @param ubicacion
     * @return
     */
    @CircuitBreaker(openTimeout=15000l, resetTimeout=30000l)
    private ResponseEntity<PronosticoClimaDTO> consultarPronosticoDelClima(Ubicacion ubicacion) {
        return retryTemplate.execute(context -> {
            log.info("Intento numero {}", context.getRetryCount());
            return invocarApiExternaPronosticoDelClima(ubicacion);
        });
    }

    /**
     * Método que se ejecuta cuando se agotan los reintentos de obtención del pronóstico del clima.
     * @param e
     * @return
     */
    @Recover
    public ResponseEntity<PronosticoClimaDTO> responderAlAgotarReintentos(Throwable e) {
        log.warn("Retorno Bad Request como respuesta al servicio de consulta del clima. La ejecucion finalizo con el error {}",e);
        return ResponseEntity.badRequest().build();
    }

    /**
     * Con la ubicacion obtengo el pronóstico de 16 días en adelante consumiendo una API externa.
     * @param ubicacion
     * @return
     */
    private ResponseEntity<PronosticoClimaDTO> invocarApiExternaPronosticoDelClima(Ubicacion ubicacion) {
        return restTemplate.exchange(
            API_WEATHER_SERVICE_URL,
            HttpMethod.GET,
            new HttpEntity(buildApiHeaders()),
            PronosticoClimaDTO.class,
            ubicacion.getLatitud(),
            ubicacion.getLongitud(),
            API_LANGUAGE,
            API_METRIC_UNITS
        );
    }

    /**
     * Configuro los encabezados necesarios para consumir la API del clima.
     * @return
     */
    private HttpHeaders buildApiHeaders() {
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(API_KEY_HEADER, API_KEY_VALUE);
        headers.set(API_HOST_HEADER, API_HOST_VALUE);

        return headers;
    }

}
