package com.santandertecnologia.challenge.service;

import com.santandertecnologia.challenge.domain.Ubicacion;
import com.santandertecnologia.challenge.repository.UbicacionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Ubicacion}.
 */
@Service
@Transactional
public class UbicacionService {

    private final Logger log = LoggerFactory.getLogger(UbicacionService.class);

    private final UbicacionRepository ubicacionRepository;

    public UbicacionService(UbicacionRepository ubicacionRepository) {
        this.ubicacionRepository = ubicacionRepository;
    }

    /**
     * Get one ubicacion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Ubicacion> findOne(Long id) {
        log.debug("Request to get Ubicacion : {}", id);
        return ubicacionRepository.findById(id);
    }

}
