package com.santandertecnologia.challenge.web.rest.errors;

public class NoExisteUbicacionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NoExisteUbicacionException(Long idUbicacion) {
        super(String.format("No existe ubicacion con el id %s",idUbicacion));
    }

}
