package com.santandertecnologia.challenge.web.rest.errors;

public class UsuarioYaEstaInscriptoEnMeetupException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UsuarioYaEstaInscriptoEnMeetupException() {
        super("El usuario ya se encontraba inscripto en la Meetup.");
    }

}
