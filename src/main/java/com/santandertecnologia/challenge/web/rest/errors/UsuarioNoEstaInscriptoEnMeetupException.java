package com.santandertecnologia.challenge.web.rest.errors;

public class UsuarioNoEstaInscriptoEnMeetupException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UsuarioNoEstaInscriptoEnMeetupException(Long idMeetup) {
        super(String.format("El usuario no esta inscripto en la meetup %s",idMeetup));
    }

}
