package com.santandertecnologia.challenge.web.rest;

import com.santandertecnologia.challenge.service.dto.BirraDTO;
import com.santandertecnologia.challenge.service.dto.MeetupDTO;
import com.santandertecnologia.challenge.service.dto.ParticipanteDTO;
import com.santandertecnologia.challenge.service.dto.TemperaturaDTO;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.net.URISyntaxException;

@Validated
@Controller
@RequestMapping("/api}")
public interface MeetupApi {

    /**
     * GET /meetups/{meetupId}/birra : Indica la cantidad de cerveza a comprar para aprovisionar la meetup.
     *
     * @param meetupId ID de la meetup de la que se quiere saber cuantas birras se necesitan comprar. (required)
     * @return Ok (status code 200)
     *         or Not Found (status code 404)
     *         or Invalid input (status code 405)
     */
    @ApiOperation(value = "Indica la cantidad de cerveza a comprar para aprovisionar la meetup.", nickname = "consultarCantidadDeBirrasAComprarParaMeetup", notes = "", response = BirraDTO.class, authorizations = {
        @Authorization(value = "jwt")
    }, tags={ "meetup-controller", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = BirraDTO.class),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/meetups/{meetupId}/birra",
        produces = { "application/json" },
        method = RequestMethod.GET)
    ResponseEntity<BirraDTO> consultarCantidadDeBirrasAComprarParaMeetup(@ApiParam(value = "ID de la meetup de la que se quiere saber cuantas birras se necesitan comprar.",required=true) @PathVariable("meetupId") Long meetupId);


    /**
     * GET /meetups/{meetupId}/temperatura : Indica la temperatura del día de la meetup.
     *
     * @param meetupId ID de la meetup de la que se quiere conocer la temperatura del día. (required)
     * @return Ok (status code 200)
     *         or Not Found (status code 404)
     *         or Invalid input (status code 405)
     */
    @ApiOperation(value = "Indica la temperatura del día de la meetup.", nickname = "consultarTemperaturaDelDiaDelMeetup", notes = "", response = TemperaturaDTO.class, authorizations = {
        @Authorization(value = "jwt")
    }, tags={ "meetup-controller", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = TemperaturaDTO.class),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/meetups/{meetupId}/temperatura",
        produces = { "application/json" },
        method = RequestMethod.GET)
    ResponseEntity<TemperaturaDTO> consultarTemperaturaDelDiaDelMeetup(@ApiParam(value = "ID de la meetup de la que se quiere conocer la temperatura del día.",required=true) @PathVariable("meetupId") Long meetupId);


    /**
     * POST /meetups : Crea una nueva meetup
     *
     * @param meetupDTO Meetup que se quiere crear (required)
     * @return Created (status code 201)
     *         or Invalid input (status code 405)
     */
    @ApiOperation(value = "Crea una nueva meetup", nickname = "createMeetup", notes = "", response = MeetupDTO.class, authorizations = {
        @Authorization(value = "jwt")
    }, tags={ "meetup-controller", })
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created", response = MeetupDTO.class),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/meetups",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<MeetupDTO> crearMeetup(@ApiParam(value = "Meetup que se quiere crear" ,required=true )  @Valid @RequestBody MeetupDTO meetupDTO) throws URISyntaxException;

    /**
     * PUT /meetups/{meetupId}/participante : Realiza el check-in de un participante en la meetup.
     *
     * @param meetupId ID de la meetup a la cual inscribirse (required)
     * @param participanteDTO Participante que quiere hacer el check-in. (required)
     * @return Created (status code 201)
     *         or Not Found (status code 404)
     *         or Invalid input (status code 405)
     */
    @ApiOperation(value = "Realiza el check-in de un participante en la meetup.", nickname = "hacerCheckinEnMeetup", notes = "", response = ParticipanteDTO.class, authorizations = {
        @Authorization(value = "jwt")
    }, tags={ "meetup-controller", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = ParticipanteDTO.class),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/meetups/{meetupId}/participante",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    ResponseEntity<ParticipanteDTO> hacerCheckinEnMeetup(@ApiParam(value = "ID de la meetup a la cual inscribirse",required=true) @PathVariable("meetupId") Long meetupId,@ApiParam(value = "Participante que quiere hacer el check-in." ,required=true )  @Valid @RequestBody ParticipanteDTO participanteDTO);

    /**
     * POST /meetups/{meetupId}/participante : Inscribe a un usuario como participante en la meetup.
     *
     * @param meetupId ID de la meetup a la cual inscribirse (required)
     * @param participanteDTO Usuario a inscribir en la meetup (required)
     * @return Created (status code 201)
     *         or Not Found (status code 404)
     *         or Invalid input (status code 405)
     */
    @ApiOperation(value = "Inscribe a un usuario como participante en la meetup.", nickname = "inscribirseAMeetup", notes = "", response = ParticipanteDTO.class, authorizations = {
        @Authorization(value = "jwt")
    }, tags={ "meetup-controller", })
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created", response = ParticipanteDTO.class),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 405, message = "Invalid input") })
    @RequestMapping(value = "/meetups/{meetupId}/participante",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<ParticipanteDTO> inscribirseAMeetup(@ApiParam(value = "ID de la meetup a la cual inscribirse",required=true) @PathVariable("meetupId") Long meetupId,@ApiParam(value = "Usuario a inscribir en la meetup" ,required=true )  @Valid @RequestBody ParticipanteDTO participanteDTO);

}
