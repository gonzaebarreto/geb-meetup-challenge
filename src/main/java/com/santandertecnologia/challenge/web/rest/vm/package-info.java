/**
 * View Models used by Spring MVC REST controllers.
 */
package com.santandertecnologia.challenge.web.rest.vm;
