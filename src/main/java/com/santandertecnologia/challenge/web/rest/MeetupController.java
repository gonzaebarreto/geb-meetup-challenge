package com.santandertecnologia.challenge.web.rest;

import com.santandertecnologia.challenge.domain.Meetup;
import com.santandertecnologia.challenge.service.MeetupService;
import com.santandertecnologia.challenge.service.dto.BirraDTO;
import com.santandertecnologia.challenge.service.dto.MeetupDTO;
import com.santandertecnologia.challenge.service.dto.ParticipanteDTO;
import com.santandertecnologia.challenge.service.dto.TemperaturaDTO;
import com.santandertecnologia.challenge.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class MeetupController implements MeetupApi {

    private final Logger log = LoggerFactory.getLogger(MeetupController.class);

    private final MeetupService meetupService;

    private static final String ENTITY_NAME = "meetup";

    public MeetupController(MeetupService meetupService) {
        this.meetupService = meetupService;
    }

    /**
     * User Story 1:
     * Como admin quiero saber cuántas cajas de birras tengo que comprar para poder aprovisionar la meetup.
     */
    @Override
    public ResponseEntity<BirraDTO> consultarCantidadDeBirrasAComprarParaMeetup(Long meetupId) {
        BirraDTO birraDTO = meetupService.consultarCantidadDeBirrasAComprar(meetupId);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(birraDTO));
    }

    /**
     * User Story 2:
     * Como admin y usuario quiero conocer la temperatura del día de la meetup para saber si va a hacer calor o no.
     */
    @Override
    public ResponseEntity<TemperaturaDTO> consultarTemperaturaDelDiaDelMeetup(Long meetupId) {
        log.debug("Id de Meetup a consultar: {}", meetupId);
        Optional<Meetup> optionalMeetup = meetupService.findOne(meetupId);
        TemperaturaDTO temperaturaDTO = null;
        if(optionalMeetup.isPresent()) {
            temperaturaDTO = optionalMeetup.get().consultarTemperatura();
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(temperaturaDTO));
    }

    /**
     * User Story 4:
     * Como admin quiero armar una meetup para poder invitar otras personas.
     *
     * @param meetupDTO Meetup que se quiere crear (required)
     * @return
     * @throws URISyntaxException
     */
    @Override
    public ResponseEntity<MeetupDTO> crearMeetup(@Valid MeetupDTO meetupDTO) throws URISyntaxException {
        if (meetupDTO.getId() != null) {
            throw new BadRequestAlertException("Una meetup nueva no puede tener un ID", ENTITY_NAME, "idexists");
        }
        if(meetupDTO.getUbicacion() == null || meetupDTO.getUbicacion().getId() == null) {
            throw new BadRequestAlertException("El id de ubicacion es obligatorio para dar de alta una meetup", "Ubicacion.id", "NotNull");
        }

        MeetupDTO meetupCreadaDTO = meetupService.crearMeetup(meetupDTO);
        return ResponseEntity.created(new URI("/api/meetups/" + meetupCreadaDTO.getId()))
            .body(meetupCreadaDTO);
    }

    /**
     * User Story 5
     * Como usuario quiero inscribirme en una meetup para poder asistir.
     *
     * @param meetupId ID de la meetup a la cual inscribirse (required)
     * @param participanteDTO Usuario a inscribir en la meetup (required)
     * @return
     */
    @Override
    @Transactional
    public ResponseEntity<ParticipanteDTO> inscribirseAMeetup(Long meetupId, @Valid ParticipanteDTO participanteDTO) {
        log.debug("Id de Meetup a consultar: {}", meetupId);
        if(participanteDTO == null || participanteDTO.noTieneValorEnIdNiEnLogin()) {
            throw new BadRequestAlertException("Debe enviar el id de usuario o login para poder inscribirse en una meetup", "user.id", "NotNull");
        }
        ParticipanteDTO parcipanteInscripto = null;
        Optional<Meetup> optionalMeetup = meetupService.findOne(meetupId);
        if (optionalMeetup.isPresent()) {
            parcipanteInscripto = meetupService.inscribirParticipanteAMeetup(optionalMeetup.get(),
                                                                             participanteDTO.getIdUsuario(),
                                                                             participanteDTO.getLogin());
        }

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parcipanteInscripto));
    }

    /**
     * User Story 6
     * Como usuario quiero hacer check-in en una meetup para poder avisar que estuve ahí.
     * @param meetupId ID de la meetup a la cual inscribirse (required)
     * @param participanteDTO Participante que quiere hacer el check-in. (required)
     * @return
     */
    @Override
    @Transactional
    public ResponseEntity<ParticipanteDTO> hacerCheckinEnMeetup(Long meetupId, @Valid ParticipanteDTO participanteDTO) {
        log.debug("Id de Meetup a consultar: {}", meetupId);
        if(participanteDTO == null || participanteDTO.getId() == null) {
            throw new BadRequestAlertException("El id de participante es obligatorio para hacer el checkin a una meetup", "participante.id", "NotNull");
        }
        Meetup meetup = meetupService.findOne(meetupId)
                                     .orElseThrow(() -> new BadRequestAlertException("El id de meetup no existe",ENTITY_NAME,"url.not.found"));

        ParticipanteDTO participanteResponseDTO = meetupService.hacerCheckingDeParticipante(meetup, participanteDTO.getId());

        return ResponseEntity.ok().body(participanteResponseDTO);
    }

}
