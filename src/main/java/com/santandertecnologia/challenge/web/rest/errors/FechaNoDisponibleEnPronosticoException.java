package com.santandertecnologia.challenge.web.rest.errors;

import java.time.LocalDate;

public class FechaNoDisponibleEnPronosticoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FechaNoDisponibleEnPronosticoException(LocalDate fecha) {
        super(String.format("La fecha %s, no se encuentra dentro del rango de fechas del pronostico del clima.",fecha));
    }

}
