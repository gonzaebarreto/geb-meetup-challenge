# Meetup Challenge

Esta aplicación fue generada inicialmente usando JHipster 6.10.4. La documentación oficial se puede encontrar en [https://www.jhipster.tech/documentation-archive/v6.10.4](https://www.jhipster.tech/documentation-archive/v6.10.4).

### Stack de Tecnología usada
* Java
* Spring-boot
* Swagger UI
* Spring Security
* Spring Retry
* Liquibase
* MySql
* Docker
* GitLab
* SonarCloud
* Heroku

## Desarrollo

Pasos para iniciar la aplicación en modo dev.

Levantar la BBDD: Usamos docker ejecutando desde línea de comandos:

```
docker-compose -f src/main/docker/mysql.yml up -d
```

Para iniciar la aplicación en modo dev hay que correr:

```
./mvnw
```

Para ver más información de cómo desarrollar con JHipster, consultar [Using JHipster in development][].

## Testing

Para lanzar los tests de la aplicación ejecutar:

```
./mvnw verify
```

For more information, refer to the [Running tests page][].

## Armado para Produccion

### Empaquetado como jar

Para armar el jar de la aplicación para Producción, correr:

```
./mvnw -Pprod clean verify
```

Para validar que el empaquetado función, correr:

```
java -jar target/*.jar
```

Consultar [Using JHipster in production][] para más detalles.

También se puede dockerizar la aplicación creando una imagen docker corriendo:

```
./mvnw -Pprod verify jib:dockerBuild
```

Luego ejecutar:

```
docker-compose -f src/main/docker/app.yml up -d
```

Para más información consultar [Using Docker and Docker-Compose][].


# User Stories
* Como admin quiero saber cuántas cajas de birras tengo que comprar para poder aprovisionar la meetup.
* Como admin y usuario quiero conocer la temperatura del día de la meetup para saber si va a hacer calor o no.
* Como usuario y como admin quiero poder recibir notificaciones para estar al tanto de las meetups.
* Como admin quiero armar una meetup para poder invitar otras personas.
* Como usuario quiero inscribirme en una meetup para poder asistir.
* Como usuario quiero hacer check-in en una meetup para poder avisar que estuve ahí.

El desarrollo de las User Stories incluye sólo la parte de Backend.

### Supuestos de negocio tenidos en cuenta para el desarrollo
* El admin también participa de la meetup, al crear la meetup se anota como participante.
* El admin también tiene perfil de Usuario.
* Siempre se pide birra para todos los participantes.
* La cerveza se puede encargar a partir de que queden 16 días para el evento, antes no se puede hacer el pedido (no hay pronóstico tan extendido).
* Tomo siempre la temperatura máxima del día, mejor que sobre y no que falte!
* La administración de nuevos usuarios queda fuera del alcance. 

Todos los usuarios y roles están generados previamente con los scripts .csv que están dentro de la carpeta src/config/liquibase/data

<img src="src/main/resources/images/LiquibaseInitData.png" width="400px" height="auto">

### Entidades de dominio de la solución del Challenge

<img src="src/main/resources/images/EntidadesDeDomino.png" width="500px" height="auto">

Todos los endpoints REST se encuentran definidos vía annotations Swagger en la interfaz MeetupApi.java
La implementación se encuentra en la clase MeetupController.

La definición de swagger se encuentra en **http://localhost:8081/swagger-ui.html**

<img src="src/main/resources/images/MeetupApi-Swagger-ui-captura.png" width="800px" height="auto">

### Autenticación
Para poder invocar a la API del Meetup hay que ejecutar con un token de acceso JWT.
Para obtener el token de acceso con usuario y password hay que invocar al endpoint **/api/authenticate**.
```
curl -i -X POST \
     -H "Content-Type: application/json" \
     -d "{\"username\":\"admin\",\"password\":\"admin\"}" \
     http://localhost:8081/api/authenticate
```
Ejemplo de respuesta con token de acceso
```
{"id_token" : "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYwMzgxNzE2OX0.KXTHOBpGiKsnjg8hNwWOqtf-jMYkUe-UkdNDN6CG6aGpyfAyi_hXiRoyQUL_xQlVlmVyUM4oWEFnjTDrdrmfUQ"}
```
A este token que hay que anteponerle el valor 'Bearer ' y enviarlo en el encabezado Authorization.
por ejemplo: Authorization: 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYwMzgxNzE2OX0.KXTHOBpGiKsnjg8hNwWOqtf-jMYkUe-UkdNDN6CG6aGpyfAyi_hXiRoyQUL_xQlVlmVyUM4oWEFnjTDrdrmfUQ'

### User Story 1:
"Como admin quiero saber cuántas cajas de birras tengo que comprar para poder aprovisionar la meetup."
### Solución:
La implementación se hace en el método consultarCantidadDeBirrasAComprarParaMeetup de MeetupController.java.
````
 GET /api/meetups/{meetupId}/birra
````

#### Aspectos técnicos 

##### Seguridad

En SecurityConfiguration se especifica que sólo se puede ejecutar este endpoint si el usuario 
tiene el rol ADMIN.

##### Retry y Circuit Breaker

Se utiliza la biblioteca Spring-retry y se implementa en la clase ClimaService.java que se utiliza para
invocar a la API externa del Clima. Esto se hace para que si comienza a fallar la API externa no se degrade 
la performance de la aplicación agotando los hilos disponibles. 

##### Tests repetibles
Los casos de tests se encuentran implementados en la clase ConsultarCajasDeBirraMeetupIT.java

Ejemplo de invocación
````
 curl http://localhost:8081/api/meetups/3/birra \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYwMzg0NzY4MH0.OYySHXtOxu41-0ggMmcYuwF6iOiy8YQDLWHa897eS9ikDxKFJnFeFU34OM4Vhlysln2NE1EoPp6qRYUjIXoz0A"
````
Respuesta
````
{
   "cajasAPedir" : 2,
   "cervezasNecesarias" : 9
 }
````

### User Story 2:
"Como admin y usuario quiero conocer la temperatura del día de la meetup para saber si va a hacer calor o no."
### Solución 
Desarrollada en el método consultarTemperaturaDelDiaDelMeetup en la clase MeetupController.java 
````
 GET /api/meetups/{meetupId}/temperatura
````
#### Aspectos técnicos 

##### Seguridad

En SecurityConfiguration se define que el endpoint sólo puede ser ejecutado si el usario tiene ROL USER ó ADMIN.

##### Retry y Circuit Breaker

Se reutiliza la misma funcionalidad para consultar el clima que se usó en la User Story 1.

##### Tests repetibles
Los casos de tests se encuentran implementados en la clase ConsultarTemperaturaMeetupIT.java

Ejemplo de invocación
````
 curl http://localhost:8081/api/meetups/3/temperatura \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYwMzg0NzY4MH0.OYySHXtOxu41-0ggMmcYuwF6iOiy8YQDLWHa897eS9ikDxKFJnFeFU34OM4Vhlysln2NE1EoPp6qRYUjIXoz0A"
````
Respuesta
````
{
  "fecha" : "2020-11-06",
  "temperatura" : 17.8,
  "descripcionClima" : "Poco nuboso"
}
````

### User Story 3:
"Como usuario y como admin quiero poder recibir notificaciones para estar al tanto de las meetups."
### Solución 

Se agrega un listener de JPA a la entidad Meetup para notificar a los usuarios que sean notificables enviandoles un mail 
indicando que se creo una meetup nueva o que se hicieron modificaciones a una existente. Considero que un usuario es 
notificable cuando está dado de alta y tiene configurado su correo electrónico.

#### Aspectos técnicos 

##### Listener de JPA sobre entity Meetup

````
@Entity
@Table(name = "meetup")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EntityListeners(MeetupListener.class)
public class Meetup implements Serializable {
````

En MeetupListener se invoca al service NotificadorEventosMeetupService que se encarga de enviar un mail a los usuarios.
Para que el mail se envíe hay que configurar en application.yml el server smtp, usuario y password. Para las pruebas 
repetibles se mockea este envío.

````
@PostPersist
    public void onPostPersist(Meetup meetup) {
        log.info("MeetupListener.onPostPersist(): " + meetup);
        SpringContext.getBean(NotificadorEventosMeetupService.class).notificarEventoMeetupNueva(meetup);
    }

    @PostUpdate
    public void onPostUpdate(Meetup meetup) {
        log.info("MeetupListener.onPostUpdate(): " + meetup);
        SpringContext.getBean(NotificadorEventosMeetupService.class).notificarEventoMeetupActualizada(meetup);
    }
````

##### Tests repetibles
Los casos de tests se encuentran en CrearMeetupIT.java.

### User Story 4:
"Como admin quiero armar una meetup para poder invitar otras personas."
### Solución 
Desarrollada en el método crearMeetup de la clase MeetupController.java 
````
 POST /api/meetups
````
#### Aspectos técnicos 

##### Seguridad

En SecurityConfiguration se define que el endpoint sólo puede ser ejecutado si el usario tiene ROL ADMIN.

##### Tests repetibles
Los casos de tests se encuentran implementados en la clase CrearMeetupIT.java

Ejemplo de invocación
````
curl -i -X POST \
      http://localhost:8081/api/meetups \
      -H "Content-Type: application/json"  \
      -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYwMzg0NzY4MH0.OYySHXtOxu41-0ggMmcYuwF6iOiy8YQDLWHa897eS9ikDxKFJnFeFU34OM4Vhlysln2NE1EoPp6qRYUjIXoz0A" \
      -d "{\"titulo\": \"Meetup de Test\",\"fechaHora\": \"2020-10-27T03:16:41.306Z\", \"duracionEnMinutos\": 360, \"ubicacion\": {\"id\": 1} }"
````
Respuesta
````
{
  "id" : 13,
  "fechaHora" : "2020-10-27T03:16:41.306Z",
  "titulo" : "Meetup de Test",
  "duracionEnMinutos" : 360,
  "ubicacion" : {
    "id" : 1,
    "nombre" : "Edificio Barracas",
    "direccion" : "Av. Paseo Colón y Av. Juan de Garay",
    "latitud" : -34.62406921386719,
    "longitud" : -58.370216369628906
  },
  "participantes" : [ {
    "id" : 45,
    "checkedIn" : false,
    "idUsuario" : 3,
    "login" : "admin",
    "nombre" : "Administrator",
    "apellido" : "Administrator",
    "email" : "admin@localhost"
  } ]
}
````

### User Story 5:
"Como usuario quiero inscribirme en una meetup para poder asistir."
### Solución 
Desarrollada en el método inscribirseAMeetup de la clase MeetupController.java 
````
 POST /api/meetups/{meetupId}/participante
````
#### Aspectos técnicos 

##### Seguridad

En SecurityConfiguration se define que el endpoint sólo puede ser ejecutado si el usario tiene ROL USER.

##### Tests repetibles
Los casos de tests se encuentran implementados en la clase InscribirseEnMeetupIT.java

Ejemplo de invocación
````
curl -i -X POST \
      http://localhost:8081/api/meetups/3/participante \
      -H "Content-Type: application/json"  \
      -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYwMzg0NzY4MH0.OYySHXtOxu41-0ggMmcYuwF6iOiy8YQDLWHa897eS9ikDxKFJnFeFU34OM4Vhlysln2NE1EoPp6qRYUjIXoz0A" \
      -d "{\"login\": \"freddiemercury\"}"
````
Respuesta
````
{
  "id" : 45,
  "checkedIn" : false,
  "idUsuario" : 6,
  "login" : "freddiemercury",
  "nombre" : "Freddie",
  "apellido" : "Mercury",
  "email" : "freddiemercury@localhost"
}
````
### User Story 6:
"Como usuario quiero hacer check-in en una meetup para poder avisar que estuve ahí."
### Solución 
Desarrollada en el método hacerCheckinEnMeetup de la clase MeetupController.java 
````
 PUT /api/meetups/{meetupId}/participante
````
#### Aspectos técnicos 

##### Seguridad

En SecurityConfiguration se define que el endpoint sólo puede ser ejecutado si el usario tiene ROL USER.

##### Tests repetibles
Los casos de tests se encuentran implementados en la clase HacerCheckinEnMeetupIT.java

Ejemplo de invocación
````
curl -i -X PUT \
      http://localhost:8081/api/meetups/3/participante \
      -H "Content-Type: application/json"  \
      -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYwMzg0NzY4MH0.OYySHXtOxu41-0ggMmcYuwF6iOiy8YQDLWHa897eS9ikDxKFJnFeFU34OM4Vhlysln2NE1EoPp6qRYUjIXoz0A" \
      -d "{\"id\": 45}"
````
Respuesta
````
{
  "id" : 45,
  "checkedIn" : true,
  "idUsuario" : 6,
  "login" : "freddiemercury",
  "nombre" : "Freddie",
  "apellido" : "Mercury",
  "email" : "freddiemercury@localhost"
}
````

# Bonus Point!
## Solución de CI/CD Propuesta
Se propone esquema de CI/CD orquestado por GitLab CI.
La configuración del pipeline propuesto se puede ver en .gitlab-ci.yml
El esquema consta de las siguientes etapas:
 * check
 * build
 * test
 * analyze
 * package
 * release
 * deploy

El proyecto está instanciado en [GitLab CI Meetup Challenge]

<img src="src/main/resources/images/GitLabPipeline.png" width="800px" height="auto">

Se versiona imagen docker de la aplicación en el Container Registry de GitLab.

<img src="src/main/resources/images/GitLabContainerRegistry.png" width="800px" height="auto">


El análisis de Sonar se puede ver en [Sonar Cloud Meetup Challenge]


<img src="src/main/resources/images/SonarCloud.png" width="800px" height="auto">


El deploy en heroku está en [Heroku Deployment]



[jhipster homepage and latest documentation]: https://www.jhipster.tech
[jhipster 6.10.4 archive]: https://www.jhipster.tech/documentation-archive/v6.10.4
[using jhipster in development]: https://www.jhipster.tech/documentation-archive/v6.10.4/development/
[using docker and docker-compose]: https://www.jhipster.tech/documentation-archive/v6.10.4/docker-compose
[using jhipster in production]: https://www.jhipster.tech/documentation-archive/v6.10.4/production/
[running tests page]: https://www.jhipster.tech/documentation-archive/v6.10.4/running-tests/
[Sonar Cloud Meetup Challenge]: https://sonarcloud.io/dashboard?id=meetupchallenge
[GitLab CI Meetup Challenge]: https://gitlab.com/gonzaebarreto/geb-meetup-challenge
[Heroku Deployment]: https://meetupchallenge.herokuapp.com/
